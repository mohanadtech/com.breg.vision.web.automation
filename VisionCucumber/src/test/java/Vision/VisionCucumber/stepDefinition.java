package Vision.VisionCucumber;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class stepDefinition {

    public static WebDriver driver = null; 
    public static String homepage = "https://uat.bregvision.com/home.aspx";
    
    @Given("^Open browser$")
    public void open_browser() throws Exception{
    	driver = new FirefoxDriver();
    	driver.manage().window().maximize();
    	driver.get(homepage);
    	Thread.sleep(2000);
    }
    
    @Given("^Enter username is \"([^\"]*)\"$")
    public void enter_Username(String username) throws Exception{
    	
    	driver.findElement(By.id("UserName")).sendKeys(username);
    	Thread.sleep(1000);
    }
    
    @Given("^Enter password is \"([^\"]*)\"$")
    public void enter_Password(String password) throws Exception{
    	
    	driver.findElement(By.id("PassWord")).sendKeys(password);
    	Thread.sleep(1000);
    }
    
    @Given("^Click on Login button$")
    public void click_Loginbutton() throws Exception{
    	
    	driver.findElement(By.id("Login")).click();
    	Thread.sleep(3000);
    }
    
    @Given("^Verify login status$")
    public void user_Logged_In() throws Exception{
    	
    	String currentPage = driver.getCurrentUrl();
    	
    	if(!currentPage.equals(homepage)){
    		System.out.println("User not yet login");
    	}
    	else{
    		System.out.println("User login successful");
    	}
    }
    
    @Then("^Select (\\d+) \"([^\"]*)\" row Product From Inventory and Enter Sugg Order Value \"([^\"]*)\"$")
    public void select_product(int product,String arg, String arg1) throws InterruptedException {
    	
    	driver.findElement(By.linkText("Home")).click();
    	
    	verify_cart();
    	
    	int suggOrdertext =  product-1;
    	List<WebElement> checkBoxList= driver.findElements(By.xpath("//input[@type='checkbox']"));
		checkBoxList.get(product).click();
		List<WebElement> suggestedOrderList= driver.findElements(By.xpath("//*[contains(@id,'_txtSuggReorderLevel_text')]"));
		suggestedOrderList.get(suggOrdertext).clear();
		suggestedOrderList.get(suggOrdertext).sendKeys(arg1);
    }
    
    @Then("^Verify Cart$")
    public void verify_cart() throws InterruptedException {
    	
    	String CartValue = driver.findElement(By.xpath("//*[@id=\"ctl00_ctlSiteCart_RadButtonItemsInCart\"]/span[2]")).getText();
    	
    	if(!CartValue.equals("CART (0 ITEMS; $0)")){
    		driver.findElement(By.id("ctl00_ctlSiteCart_RadButtonItemsInCart")).click();
    		
    		Thread.sleep(3000);
    		
    		driver.findElement(By.id("ctl00_MainContent_btnDeleteAll")).click();
    		WebElement clickHomeLink = (new WebDriverWait(driver, 20))
					  .until(ExpectedConditions.presenceOfElementLocated(By.linkText("Home")));
			clickHomeLink.click();
			
			Thread.sleep(3000);
    	}
    	else{
    		System.out.println("Cart is empty");
    	}
    	
    }
    
    @Then("^Click Add To Cart$")
    public void click_AddToCart() throws Exception {
    	driver.findElement(By.id("ctl00_MainContent_btnAddItemsReOrderGrid")).click();
    	
    	Thread.sleep(3000);
    }
    
    @Then("^Click on Cart Button$")
    public void click_Cart() throws Exception {
    	
    	String CartValue = driver.findElement(By.xpath("//*[@id=\"ctl00_ctlSiteCart_RadButtonItemsInCart\"]/span[2]")).getText();
    	
    	if(!CartValue.equals("CART (0 ITEMS; $0)")){
    		driver.findElement(By.id("ctl00_ctlSiteCart_RadButtonItemsInCart")).click();
    		
    		Thread.sleep(3000);
    	}
    	else{
    		System.out.println("Cart is empty");
    	}
    }
    
    @Then("^Click on Purchase Button$")
    public void click_Purchase() throws Exception {
    	driver.findElement(By.id("ctl00_MainContent_Purchase")).click();
    	
    	Thread.sleep(3000);
    }
    
    @Then("^Click on Next Button$")
    public void click_Next() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_btnNext")).click();
    	
    	Thread.sleep(3000);
    }
    
    @Then("^Click on Submit Order$")
    public void click_Submit_Order() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_btnSubmitOrder")).click();
    	
    	Thread.sleep(3000);
    }
    
    @Then("^Click on Admin$")
    public void click_Admin() throws Exception{
    	driver.findElement(By.linkText("Admin")).click();
    	
    	Thread.sleep(2000);
    }
    
    @Then("^Click on Users$")
    public void click_Users() throws Exception{
    	driver.findElement(By.linkText("Users")).click();
    	
    	Thread.sleep(2000);
    }
    
    @Then("^Click on New User$")
    public void click_New_User() throws Exception{
    	
    	WebElement clickNewUserLink = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.linkText("New User")));
		clickNewUserLink.click();
		
		Thread.sleep(2000);
    }
    
    @Then("^Enter the User Information$")
    public void click_Enter_Information() throws Exception{
    	
    	WebElement enterUserName = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_ctl00_MainContent_MainContent_ctlPracticeUsers_grdPracticeUser_ctl00_ctl02_ctl03_txtUserName")));
		enterUserName.sendKeys("uattest");
		
		
		Thread.sleep(2000);
		
		WebElement enterUserEmail = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_ctl00_MainContent_MainContent_ctlPracticeUsers_grdPracticeUser_ctl00_ctl02_ctl03_txtEmail")));
		enterUserEmail.sendKeys("uattest@adtechcorp.in");
		
		Thread.sleep(2000);
		
		WebElement enterUserPassword = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_ctl00_MainContent_MainContent_ctlPracticeUsers_grdPracticeUser_ctl00_ctl02_ctl03_txtPassword")));
		enterUserPassword.sendKeys("Breg2007!");
		
		Thread.sleep(2000);
		
		WebElement enterConfirmPassword = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_ctl00_MainContent_MainContent_ctlPracticeUsers_grdPracticeUser_ctl00_ctl02_ctl03_txtPasswordCompare")));
		enterConfirmPassword.sendKeys("Breg2007!");
		
		Thread.sleep(2000);
	
    }
    
    @Then("^Click on Update$")
    public void click_Update() throws Exception{
    	
    	WebElement updateButton = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_ctl00_MainContent_MainContent_ctlPracticeUsers_grdPracticeUser_ctl00_ctl02_ctl03_btnUpdate")));
		updateButton.click();
		
		Thread.sleep(2000);
    }
    
    @Then("^Click on Check-In$")
    public void click_CheckIn() throws Exception{
    	WebElement CheckIn = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.linkText("Check-In")));
    	CheckIn.click();
    	///driver.findElement(By.linkText("Check-In")).click();
		Thread.sleep(3000);
    }
    
    @Then("^Click on Purchase Order Header$")
    public void click_Purchase_Order() throws Exception{
    	WebElement PurchaseOrder = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.linkText("Purchase Order")));
    	PurchaseOrder.click();
    	//driver.findElement(By.linkText("Purchase Order")).click();
		Thread.sleep(3000);
    }
    
    @Then("^Click on Purchase Order For Desc$")
    public void click_Purchase_Order_Desc() throws Exception{
    	WebElement PurchaseOrder = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.linkText("Purchase Order")));
    	PurchaseOrder.click();
    	//driver.findElement(By.linkText("Purchase Order")).click();
		Thread.sleep(3000);
    }
    
    @Then("^Click on Expand Purchase Order$")
    public void click_Expand_Purchase_Order() throws Exception{
    	
    	WebElement selectItemForCheckIn = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ctl00_MainContent_ctlCheckInPoOutstanding_grdPOOutstanding_ctl00_ctl04_GECBtnExpandColumn\"]")));
		selectItemForCheckIn.click();
		Thread.sleep(3000);
    }
    
    @Then("^Click on Expand Invoice Tab$")
    public void click_Expand_Invoice_Tab() throws Exception{
    	
    	WebElement selectInnerProduct = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ctl00_MainContent_ctlCheckInPoOutstanding_grdPOOutstanding_ctl00_ctl06_Detail10_ctl04_GECBtnExpandColumn\"]")));
		selectInnerProduct.click();
		Thread.sleep(2000);
    }
    
    @Then("^Click on Selected Product Checkbox$")
    public void click_Selected_Product_Checkbox() throws Exception{
    	WebElement selectCheckboxForCheckIn = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ctl00_MainContent_ctlCheckInPoOutstanding_grdPOOutstanding_ctl00_ctl06_Detail10_ctl06_Detail10_ctl02_ctl01_ClientSelectColumnSelectCheckBox\"]")));
		selectCheckboxForCheckIn.click();
		Thread.sleep(2000);
    }
    
    @Then("^Click on Check-in Products Button$")
    public void click_CheckIn_Products_Button() throws Exception{
    	WebElement clickCheckInProductsButton = (new WebDriverWait(driver, 20))
				  .until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_MainContent_ctlCheckInPoOutstanding_btnCheckInProducts")));
		clickCheckInProductsButton.click();
    	Thread.sleep(2000);
    }
    
    @Then("^Search Product Code \"([^\"]*)\"$")
    public void search_Product(String productCode) throws Exception{
    	driver.findElement(By.linkText("Home")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_RadToolbar1_rttbFilter_ctl00_tbFilter_text")).sendKeys(productCode);
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_RadToolbar1_rtbFilter")).click();
    	Thread.sleep(4000);
    }
    
    @Then("^Select Product and Add To Cart$")
    public void select_Product() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_grdSearch_ctl00_ctl04_ClientSelectColumnSelectCheckBox")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_btnAddItemsSearch")).click();
    	Thread.sleep(4000);
    }
    
    @Then("^Fill the Custom Brace Details$")
    public void enter_CustomBrace_Details() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_PO")).sendKeys("mohan");
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_Patient Name")).sendKeys("Test");
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_Diagnosis")).sendKeys("Batchu");
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_Age")).sendKeys("40");
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_Height inches")).sendKeys("171");
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_Weight lbs")).sendKeys("72");
    	Thread.sleep(2000);
    }
    
    @Then("^Click on Custom Brace Next Button$")
    public void click_CustomBrace_Next_Button() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_CustomBrace_btnNext")).click();
    	Thread.sleep(2000);
    	
    }
    
    @Then("^Click on Submit Order Button$")
    public void click_Submit_Order_Button() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_btnSubmitOrder")).click();
    	Thread.sleep(2000);
    	
    }
   
    @Then("^Click on Dispense$")
    public void click_Dispense_Button() throws Exception{
    	driver.findElement(By.linkText("Dispense")).click();
    	Thread.sleep(2000);
    	
    }
    
    @Then("^Select a product$")
    public void select_product() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_grdInventory_ctl00_ctl04_GECBtnExpandColumn")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_grdInventory_ctl00_ctl06_Detail10_ctl04_ClientSelectColumnSelectCheckBox")).click();
    	Thread.sleep(2000);
    	
    }
    
    @Then("^Click on Add To Dispense$")
    public void click_Add_Dispense_Button() throws Exception{
    	driver.findElement(By.id("ctl00_MainContent_btnAddBrowseToDispensement")).click();
    	Thread.sleep(2000);
    	
    }
  
    @Then("^Enter the Dispensement Details$")
    public void click_Add_Dispense_Details() throws Exception{
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_grdSummary2_ctl00_ctl04_GECBtnExpandColumn")).click();
    	Thread.sleep(2000);
    	Select dropdown = new Select(driver.findElement(By.id("ctl00_MainContent_grdSummary2_ctl00_ctl06_Detail10_ctl04_cboPhysicians")));
    	dropdown.selectByVisibleText("Boris Doc M.D.");
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_grdSummary2_ctl00_ctl06_Detail10_ctl04_txtPatientFirstName")).sendKeys("Mohan");
    	Thread.sleep(1000);
    	driver.findElement(By.id("ctl00_MainContent_grdSummary2_ctl00_ctl06_Detail10_ctl04_txtPatientLastName")).sendKeys("Kumar");
    	Thread.sleep(1000);
    	driver.findElement(By.id("ctl00_MainContent_grdSummary2_ctl00_ctl06_Detail10_ctl04_txtPatientCode")).sendKeys("Batchu");
    	Thread.sleep(1000);
    	driver.findElement(By.id("ctl00_MainContent_grdSummary2_ctl00_ctl04_chkRowSelectSelectCheckBox")).click();
    	Thread.sleep(1000);
    	
    }
    
    @Then("^Click on Dispense product$")
    public void click_Dispense() throws Exception{
    	Thread.sleep(2000);
    	driver.findElement(By.id("ctl00_MainContent_btnDispenseProducts")).click();
    	Thread.sleep(2000);
    	
    }
   
}
