package Vision.Functions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utilities {
	
	public static WebDriver driver = null;
    public static String homepage = "https://uat.bregvision.com/home.aspx";
    
    public static void browser_Invoke(){
    	
    	driver = new FirefoxDriver();
    	driver.manage().window().maximize();
    }
}
