Feature: Vision Testing
	
	Scenario:  Login to the application
		 Given User is Logged In
	
	Scenario: Add Items To Cart
		 Given User is Logged In
    	 Then Select 4 "th" row Product From Inventory and Enter Sugg Order Value "4"
    	 And Click Add To Cart
    	 
    Scenario: Purchase Cart Items
    	 Given User is Logged In
    	 Then Click on Cart Button
    	 Then Click on Purchase Button
    	 Then Click on Next Button
    	 And Click on Submit Order
    	 
    Scenario: Items Check-in 
    	 Given User is Logged In
    	 Then Click on Check-In
    	 Then Click on Purchase Order Header
    	 Then Click on Purchase Order For Desc
    	 Then Click on Expand Purchase Order
    	 Then Click on Expand Invoice Tab
    	 Then Click on Selected Product Checkbox
    	 And Click on Check-in Products Button
    
    Scenario: Add Custom Brace to Cart and Purchase Order
    	 Given User is Logged In
    	 Then Verify Cart 
    	 Then Search Product Code "00028"
    	 Then Select Product and Add To Cart
    	 Then Click on Cart Button
    	 Then Click on Purchase Button
    	 Then Fill the Custom Brace Details
    	 Then Click on Custom Brace Next Button
    	 And Click on Submit Order
    	 
    Scenario: Create a user
    	 Given User is Logged In
    	 Then Click on Admin
    	 Then Click on Users
    	 Then Click on New User
    	 Then Enter the User Information
    	 And Click on Update
    	 
    Scenario: Create a dispense
    	 Given User is Logged In
    	 Then Click on Dispense
    	 Then Select a product 
    	 Then Click on Add To Dispense
    	 Then Enter the Dispensement Details
    	 And Click on Dispense a product
    	 
    
    	 
        
        
	