$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("/Users/mohankumar/Documents/AdtechAutomation/VisionCucumber/src/test/java/login.feature");
formatter.feature({
  "line": 1,
  "name": "Vision Testing",
  "description": "",
  "id": "vision-testing",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 5,
  "name": "Create a dispense",
  "description": "",
  "id": "vision-testing;create-a-dispense",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "User is Logged In",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Click on Dispense",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Select a product",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Click on Add To Dispense",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Enter the Dispensement Details",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "Click on Dispense a product",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefinition.user_Logged_In()"
});
formatter.result({
  "duration": 23501541916,
  "status": "passed"
});
formatter.match({
  "location": "stepDefinition.click_Dispense_Button()"
});
formatter.result({
  "duration": 2213777295,
  "status": "passed"
});
formatter.match({
  "location": "stepDefinition.select_product()"
});
formatter.result({
  "duration": 7778357778,
  "status": "passed"
});
formatter.match({
  "location": "stepDefinition.click_Add_Dispense_Button()"
});
formatter.result({
  "duration": 2121467080,
  "status": "passed"
});
formatter.match({
  "location": "stepDefinition.click_Add_Dispense_Details()"
});
formatter.result({
  "duration": 9810499785,
  "status": "passed"
});
formatter.match({
  "location": "stepDefinition.click_Dispense()"
});
formatter.result({
  "duration": 2108129355,
  "status": "passed"
});
});